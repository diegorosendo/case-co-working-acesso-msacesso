package br.com.itau.acesso.client.msporta;

public class PortaClientFallBack implements PortaClient {
    @Override
    public PortaClientRequest getById(long idCliente) {
        throw new PortaOfflineException();
    }
}
