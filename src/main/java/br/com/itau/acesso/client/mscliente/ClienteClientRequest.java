package br.com.itau.acesso.client.mscliente;

public class ClienteClientRequest {

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return nome;
    }

    public void setName(String nome) {
        this.nome = nome;
    }

    private Long id;
    private String nome;
}
