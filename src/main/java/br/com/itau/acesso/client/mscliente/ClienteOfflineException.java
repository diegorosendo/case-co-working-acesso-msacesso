package br.com.itau.acesso.client.mscliente;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O micro serviço cliente está offline.")
public class ClienteOfflineException extends RuntimeException {
}
