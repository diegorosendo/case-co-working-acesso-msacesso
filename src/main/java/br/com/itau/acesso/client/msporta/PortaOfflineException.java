package br.com.itau.acesso.client.msporta;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O micro serviço porta está offline.")
public class PortaOfflineException extends RuntimeException {
}
