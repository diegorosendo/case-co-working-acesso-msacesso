package br.com.itau.acesso.client.mscliente;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente", configuration = ClienteClientConfiguration.class)
public interface ClienteClient {

    @GetMapping("cliente/{idCliente}")
    ClienteClientRequest getById(@PathVariable long idCliente);

}
