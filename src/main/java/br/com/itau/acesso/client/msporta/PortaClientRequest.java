package br.com.itau.acesso.client.msporta;

public class PortaClientRequest {

    private long id;
    private String andar;
    private String sala;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAndar() {
        return andar;
    }

    public void setAndar(String andar) {
        this.andar = andar;
    }

    public String getSala() {
        return sala;
    }

    public void setSala(String sala) {
        this.sala = sala;
    }

    public PortaClientRequest() {
    }
}
