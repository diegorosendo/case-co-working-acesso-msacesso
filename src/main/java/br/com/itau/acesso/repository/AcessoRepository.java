package br.com.itau.acesso.repository;

import br.com.itau.acesso.model.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AcessoRepository extends CrudRepository<Acesso, Long> {


    Optional<Acesso> findByClienteIdAndPortaId(Long clienteId, Long portaId);

    Boolean deleteByClienteIdAndPortaId(Long clienteId, Long portaId);

}
