package br.com.itau.acesso.controller;

import br.com.itau.acesso.model.Acesso;
import br.com.itau.acesso.model.dto.AcessoMapper;
import br.com.itau.acesso.model.dto.CreateAcessoRequest;
import br.com.itau.acesso.model.dto.CreateAcessoResponse;
import br.com.itau.acesso.model.dto.GetAcessoResponse;
import br.com.itau.acesso.service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

//import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    private AcessoService acessoService;

    @Autowired
    private AcessoMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateAcessoResponse create(@Valid @RequestBody CreateAcessoRequest createAcessoRequest) {
//    public CreateAcessoResponse create(@RequestBody CreateAcessoRequest createAcessoRequest) {
        Acesso acesso = mapper.toAcesso(createAcessoRequest);

        acesso = acessoService.create(acesso);

        return mapper.toCreatedAcessoResponse(acesso);
    }

    @DeleteMapping("/{clienteId}/{portaId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteByClienteIdAndPortaId(@PathVariable("clienteId") Long clienteId, @PathVariable("portaId") Long portaId) {
        acessoService.deleteByClienteIdAndPortaId(clienteId, portaId);
    }

    @GetMapping("/{clienteId}/{portaId}")
    public GetAcessoResponse getByClienteIdAndPortaId(@PathVariable("clienteId") Long clienteId, @PathVariable("portaId") Long portaId) {


        Acesso acesso = acessoService.getByClienteIdAndPortaId(clienteId, portaId);
        return mapper.toGetAcessoResponse(acesso);
    }
//
//    @GetMapping
//    public GetCreditCardResponse getById(@RequestParam(value = "id" , required = true)  long id) {
//        CreditCard creditCard = creditCardService.getById(id);
//        return mapper.toGetCreditCardResponse(creditCard);
//    }
}
