package br.com.itau.acesso.service;

import br.com.itau.acesso.client.mscliente.ClienteClient;
import br.com.itau.acesso.client.mscliente.ClienteClientRequest;
import br.com.itau.acesso.client.msporta.PortaClient;
import br.com.itau.acesso.client.msporta.PortaClientRequest;
import br.com.itau.acesso.exceptions.AcessoJaCadastradoException;
import br.com.itau.acesso.exceptions.AcessoNotFoundException;
import br.com.itau.acesso.model.Acesso;
import br.com.itau.acesso.model.dto.ChecaAcesso;
import br.com.itau.acesso.repository.AcessoRepository;
import net.bytebuddy.asm.Advice;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class AcessoService {

    @Autowired
    private AcessoRepository acessoRepository;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PortaClient portaClient;


    public Acesso create(Acesso acesso) {

        PortaClientRequest portaClientById = portaClient.getById(acesso.getPortaId());
        acesso.setPortaId(portaClientById.getId());


        ClienteClientRequest clienteClientById = clienteClient.getById(acesso.getClienteId());
        acesso.setClienteId(clienteClientById.getId());

        try {
            acesso = acessoRepository.save(acesso);
        }catch (DataIntegrityViolationException e){
            throw new AcessoJaCadastradoException();
        }catch (Exception e){
            System.out.println("Deu exception!");
            System.out.println(e.getClass().getName());
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw e;
        }

        return  acesso;
    }

    public void deleteByClienteIdAndPortaId(Long clienteId, Long portaId) {

        acessoRepository.delete(getByClienteIdAndPortaId(clienteId, portaId));

    }

    public Acesso getByClienteIdAndPortaId(Long clienteId, Long portaId) {

        Acesso acesso = new Acesso();

        PortaClientRequest portaClientById = portaClient.getById(portaId);
        acesso.setPortaId(portaClientById.getId());

        ClienteClientRequest clienteClientById = clienteClient.getById(clienteId);
        acesso.setClienteId(clienteClientById.getId());

        Optional<Acesso> byClienteIdAndPortaId = acessoRepository.findByClienteIdAndPortaId(acesso.getClienteId(), acesso.getPortaId());

        if(!byClienteIdAndPortaId.isPresent()) {
            // Grava log Kafka FALSE --> Acesso Não liberado
            enviarAoKafka(acesso, false);
            throw new AcessoNotFoundException();
        }

        acesso = byClienteIdAndPortaId.get();
        // Grava log Kafka TRUE --> Acesso liberado
        enviarAoKafka(acesso, true);

        return acesso;
    }

    @Autowired
    private KafkaTemplate<String, ChecaAcesso> producer;

    public void enviarAoKafka(Acesso acesso, Boolean acessoLiberado)
    {
        ChecaAcesso checaAcesso = new ChecaAcesso();
        checaAcesso.setAcessoLiberado(acessoLiberado);
        checaAcesso.setClienteId(acesso.getClienteId());
        checaAcesso.setPortaId(acesso.getPortaId());
        checaAcesso.setDataHora(LocalDateTime.now());
        producer.send("spec3-diego-rosendo-1", checaAcesso);
    }


}
