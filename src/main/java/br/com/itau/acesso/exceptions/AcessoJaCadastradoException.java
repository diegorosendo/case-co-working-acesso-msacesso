package br.com.itau.acesso.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Acesso já está cadastrado.")
public class AcessoJaCadastradoException extends RuntimeException{
}
