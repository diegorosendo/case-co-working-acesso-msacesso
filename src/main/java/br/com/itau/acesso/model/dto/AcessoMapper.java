package br.com.itau.acesso.model.dto;

import br.com.itau.acesso.model.Acesso;
import org.springframework.stereotype.Component;

@Component
public class AcessoMapper {

    public Acesso toAcesso(CreateAcessoRequest createAcessoRequest) {
        Acesso creditCard = new Acesso();
        creditCard.setClienteId(createAcessoRequest.getCliente_id());
        creditCard.setPortaId(createAcessoRequest.getPorta_id());

        return creditCard;
    }

    public CreateAcessoResponse toCreatedAcessoResponse(Acesso acesso) {
        CreateAcessoResponse createAcessoResponse = new CreateAcessoResponse();

        createAcessoResponse.setCliente_id(acesso.getClienteId());
        createAcessoResponse.setPorta_id(acesso.getPortaId());

        return createAcessoResponse;
    }


    public GetAcessoResponse toGetAcessoResponse(Acesso acesso) {
        GetAcessoResponse getAcessoResponse = new GetAcessoResponse();

        getAcessoResponse.setCliente_id(acesso.getClienteId());
        getAcessoResponse.setPorta_id(acesso.getPortaId());

        return getAcessoResponse;
    }
}
