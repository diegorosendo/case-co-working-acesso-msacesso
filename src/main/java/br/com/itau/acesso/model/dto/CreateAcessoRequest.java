package br.com.itau.acesso.model.dto;

//import com.fasterxml.jackson.annotation.JsonProperty;


import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class CreateAcessoRequest {

    @NotNull
    private Long porta_id;

    @NotNull
    private Long cliente_id;

    public CreateAcessoRequest() {
    }

    public Long getPorta_id() {
        return porta_id;
    }

    public void setPorta_id(Long porta_id) {
        this.porta_id = porta_id;
    }

    public Long getCliente_id() {
        return cliente_id;
    }

    public void setCliente_id(Long cliente_id) {
        this.cliente_id = cliente_id;
    }
}
